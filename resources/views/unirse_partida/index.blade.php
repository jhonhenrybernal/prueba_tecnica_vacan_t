@extends('layouts.app')

@section('title', 'Unirse a la partida')
@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">Unirse partida</h1>
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            <form  action="/partida/codigo" method="post">
                @csrf
                <div class="col-auto">
                    <label for="codigo" class="visually">Codigo</label>
                    <input type="password" class="form-control" id="codigo" name="codigo" placeholder="Codigo" value="">
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary mb-2">Continuar</button>
                </div>
            </form>
        </div>
    </div>
    <script>

        function jugar(params) {

            document.getElementById('tipo-partida').value = params
            document.getElementById('continuar').submit()
        }
    </script>
@endsection
