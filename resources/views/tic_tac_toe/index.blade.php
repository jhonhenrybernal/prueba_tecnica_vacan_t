

@extends('layouts.app')

@section('title', 'Iniciar')
@section('content')
    <input type="hidden" id="status_game" value="">
    <div id="alert"></div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token"/>
    <h1>Bienvenido {{($bienvenida == 'creador')? $jugadorCreador:$jugadorInvitado }}</h1>
    <h3>Contrincante  {{($bienvenida == 'creador')? $jugadorInvitado :$jugadorCreador }}</h3>
    <input type="hidden" id="codigo" name="codigo" value="{{ $codigo }}">
    <input type="hidden" name="tipo_jugador" id="tipo_jugador"  @if($bienvenida == 'creador') value="creador" @else value="invitado" @endif >

    <h3>Tu codigo: {{ $codigo }}</h3>
      @csrf
    <div class="container-game" id='container-game'>

    </div>
    <div id="button"></div>

@endsection
