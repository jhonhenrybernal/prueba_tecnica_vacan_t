
@extends('layouts.app')

@section('title', 'Bienvenidos-jugemos')
@section('content')

    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">Bienvenidos tic tac toe</h1>
            <a href="{{route('nueva_partida')}} " class="btn btn-primary my-2">Nueva partida</a>
            <a href="{{route('unirse_partida')}}" class="btn btn-secondary my-2">Unirse partida</a>
            </p>
        </div>
    </div>
@endsection
