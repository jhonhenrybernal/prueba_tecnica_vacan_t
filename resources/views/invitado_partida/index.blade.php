
@extends('layouts.app')

@section('title', 'Unirse a la partida')
@section('content')

    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">Bienvenido invitado</h1>
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            <form class="row g-3" action="/partida/crear/invitado" method="post">
                    @csrf
                <label for="nombre" class="visually">Nombre jugador</label>
            <div class="col-auto">
                <input type="hidden"  name="codigo" value="{{ $codigo }}">
                <input type="text"  class="form-control" id="nombre_jugador_creador" name="nombre_jugador_invitado" value="">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-3">Crear</button>
            </div>
            </form>
        </div>
    </div>
@endsection
