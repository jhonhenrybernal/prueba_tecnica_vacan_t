@extends('layouts.app')

@section('title', 'Unirse a la partida')
@section('content')

    <div class="row">
        <div class="col-lg-6 col-md-8 mx-auto">
               <h1 class="fw-light">Seleccione jugador</h1>
                @if (isset($exception))
                    <h2>{{ $exception->getMessage() }}</h2>
                @endif
                <form action="/partida/seleccion" method="post" id="continuar">
                    @csrf

                    <input type="hidden"  id="tipo-partida" name="tipo_partida" value="">
                    <input type="hidden"  name="codigo" value="{{ $codigo }}">
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" value="" onclick="jugar('creador')" id="inlineCheckbox1">
                    <label class="form-check-label" for="inlineCheckbox1" >{{ $jugadorCreador }} &nbsp; o</label>
                    </div>
                    @if (empty($jugadorInvitado))
                    <button type="button" class="btn btn-primary" onclick="jugar('invitado')">Soy invitado</button>
                    @else
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="" onclick="jugar('invitado')" >
                        <label class="form-check-label" for="inlineCheckbox2" >{{ $jugadorInvitado }} </label>
                    </div>
                    @endif
                </form>
        </div>
    </div>
    <script>

        function jugar(params) {

            document.getElementById('tipo-partida').value = params
            document.getElementById('continuar').submit()
        }
    </script>
@endsection
