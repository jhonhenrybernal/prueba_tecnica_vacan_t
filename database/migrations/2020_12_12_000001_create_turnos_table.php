<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->string('jugador');
            $table->string('posicion');
            $table->string('seleccion');
            $table->string('turno_actual');
            $table->timestamps();
        });
    }
}
