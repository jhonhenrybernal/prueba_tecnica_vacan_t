<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas', function (Blueprint $table) {
            $table->id();
            $table->string('codigo',1000);
            $table->string('nombre_jugador_creador');
            $table->string('nombre_jugador_invitado')->nullable(true);
            $table->boolean('status');
            $table->string('ganador')->nullable(true);
            $table->timestamps();
        });
    }
}
