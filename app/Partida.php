<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    protected $table = 'partidas';
    protected $fillable = [
        'codigo',
        'nombre_jugador_creador',
        'nombre_jugador_invitado',
        'ganador',
        'status'
    ];

}
