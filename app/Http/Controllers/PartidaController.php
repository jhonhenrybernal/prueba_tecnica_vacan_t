<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partida;
use App\Turnos;

class PartidaController extends Controller
{

    //creacion de partida
   public function nuevoCreador(Request $request){

        $partida = Partida::where('nombre_jugador_creador', $request['nombre_jugador_creador'])->first();
        if (!empty($partida)) {
            $exception = 'Nombre ya existe, utilice otro';
            return back()->withError($exception)->withInput();
        }

        $codigo = $this->createCode();
        $partidas = new Partida();
        $partidas->codigo = $codigo;
        $partidas->nombre_jugador_creador = $request['nombre_jugador_creador'];
        $partidas->nombre_jugador_invitado = '';
        $partidas->status = false;
        $partidas->ganador = '';
        $partidas->save();

        return redirect()->action(
            [PartidaController::class, 'iniciar'],
            ['codigo' => $codigo,
             'bienvenida' => 'creador']
        );
    }

    //Cuando existe partida y invitado no tiene no tiene nombre
    public function nuevoInvitado(Request $request){
        $partida = Partida::where('codigo', $request['codigo'])->first();
        $partida->nombre_jugador_invitado = $request['nombre_jugador_invitado'];
        $partida->update();

        return redirect()->action(
            [PartidaController::class, 'iniciar'],
            ['codigo' => $request['codigo'],
             'bienvenida' => 'invitado']
        );
    }

    // ingreso de codigo cuando ya existe una partida
    public function codigo(Request $request){
        $partida = Partida::where('codigo', $request['codigo'])->first();
        if (empty($partida)) {
            $exception = 'Codigo no existe';
            return back()->withError($exception)->withInput();
        }
        $codigo = $partida['codigo'];
        $jugadorCreador = $partida['nombre_jugador_creador'];
        $jugadorInvitado = $partida['nombre_jugador_invitado'];
        return response()->view('seleccion_partida.index', compact( 'codigo', 'jugadorCreador', 'jugadorInvitado'));
    }

    //despues de crear como creador o invitado acceder por este metodo
    public function unirse(Request $request){
        $partida = Partida::where('codigo', $request['codigo'])->first();

        if ($request['tipo_partida'] == 'creador') {
            if (empty($partida['nombre_jugador_invitado'])) {
                $exception = 'Aun no tenemos invitado';
                return back()->withError($exception)->withInput();
            }
            return redirect()->action(
                [PartidaController::class, 'iniciar'],
                ['codigo' => $partida['codigo'],
                    'bienvenida' => 'creador']
            );

        }
        if ($request['tipo_partida'] == 'invitado') {
            $codigo = $partida['codigo'];
            if (!empty($partida['nombre_jugador_invitado']) && !empty($partida['nombre_jugador_creador'])) {
                return redirect()->action(
                    [PartidaController::class, 'iniciar'],
                    [
                        'codigo' => $request['codigo'],
                        'bienvenida' => 'invitado'
                    ]
                );
            }
            return response()->view('invitado_partida.index', compact('codigo'));
        }
    }

    // todo proceso acceder desde aca
    public function iniciar($codigo, $bienvenida){

        $partida = Partida::where('codigo', $codigo)->first();
        $turno = Partida::where('codigo', $codigo)->first();
        $jugadorCreador = $partida['nombre_jugador_creador'];
        $jugadorInvitado = $partida['nombre_jugador_invitado'];

        return response()->view('tic_tac_toe.index',compact('codigo', 'jugadorCreador', 'bienvenida', 'jugadorInvitado', 'turno'));
    }

    //generador de codigo
    public function createCode()
    {

        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((float)microtime() * 1000);
        $i = 0;
        $pass = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;
    }


    //proceso de jugada paso a paso
    public function turnos(Request $request){
        $turnos = Turnos::where('codigo', $request['codigo'])->andWhere('posicion', $request['codigo'])->first();
        if (!$turnos) {
            return response()->json([
                'status' => false

            ]);
        }
        $turno = new Turnos();
        $turno->codigo = $request['codigo'];
        $turno->jugador = $request['turno'];
        $turno->posicion = $request['numero'];
        $turno->seleccion = $request['turno'] == 'creador'?'x':'o';
        $turno->turno_actual = 'turno';
        $turno->save();
    }

    public function listar($codigo){
        return response()->json([
            'data' => Turnos::where('codigo',$codigo)->get()
        ]);
    }
}
