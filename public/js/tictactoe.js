
window.onload = function () {
   startGame()

}

//renderizar la partida
function startGame() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", '/listar/' + document.getElementById('codigo').value , false); // false for synchronous request
    xmlHttp.send(null);
    console.log(xmlHttp.responseText);
    var gm = document.getElementById('container-game');
    gm.innerHTML = "<div class='row border-b'><div class='col border-r ' onclick='ticTacToeGame(1)' id='posicion1' ></div><div class='col border-r ' onclick='ticTacToeGame(2)' id='posicion2'></div><div class='col ' onclick='ticTacToeGame(3)' id='posicion3'></div></div ><div class='row border-b'><div class='col border-r' onclick='ticTacToeGame(4)' id='posicion4'></div><div class='col border-r' onclick='ticTacToeGame(5)' id='posicion5'></div><div class='col ' onclick='ticTacToeGame(6)' id='posicion6'></div></div><div class='row'><div class='col border-r' onclick='ticTacToeGame(7)' id='posicion7'></div><div class='col border-r' onclick='ticTacToeGame(8)' id='posicion8'></div><div class='col' onclick='ticTacToeGame(9)' id='posicion9'></div></div>"
}

//paso a juego al backend
function ticTacToeGame(numero) {
    var tic = document.getElementById('posicion' +numero);
    var status = document.getElementById('status_game');
    var alerta = document.getElementById('alert');
    if (status.value == 'true') {
        alert('Ya tuviste tu turno')
    }else{
        var turno = document.getElementById('tipo_jugador').value
        if (turno == 'creador') {
            tic.innerHTML = 'x'
        }else{
            tic.innerHTML = 'o'
        }
        status.value = true
        alerta.innerHTML = '<div class="alert alert-primary" role="alert">Espera tu turno....</div >'
        fetch("/partida/turnos", {
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "_token": document.getElementById('token').value
            },
            method: "post",
            credentials: "same-origin",
            body: JSON.stringify({
                'numero': numero,
                "_token": document.getElementById('token').value,
                'codigo': document.getElementById('codigo').value,
                'turno': document.getElementById('tipo_jugador').value,
            })
        }).then(response => response.json())

    }
}


