<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\PartidaController;



Route::get('/', function () {
    return view('welcome');
});
Route::get('/partida/nueva', function () {
    return view('nueva_partida.index');
})->name('nueva_partida');

Route::get('/partida/unirse', function () {
    $inicio = true;
    return view('unirse_partida.index',compact('inicio'));
})->name('unirse_partida');

Route::post('/partida/seleccion', [PartidaController::class, 'unirse'])->name('seleccion_partida');
Route::get('/partida/iniciar/{codigo}/{bienvenida}' , [PartidaController::class, 'iniciar'])->name('iniciar_partida');


Route::post('/partida/crear/creador', [PartidaController::class, 'nuevoCreador'])->name('crear_partida');
Route::post('/partida/crear/invitado', [PartidaController::class, 'nuevoInvitado'])->name('crear_partida_invitado');


Route::post('/partida/turnos', [PartidaController::class, 'turnos'])->name('turnos_partida');
Route::post('/partida/codigo', [PartidaController::class, 'codigo'])->name('tipo_artida_partida');


Route::get('/listar/{codigo}', [PartidaController::class, 'listar']);
